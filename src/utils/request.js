import axios from "axios";
import { ElMessage } from "element-plus";
import store from "./store";


axios.defaults.baseURL = import.meta.env.VITE_API_BASE_URL;

axios.defaults.timeout = 15000;//15s
axios.defaults.withCredentials = true;
// axios.defaults.headers.post["Content-Type"] ="application/x-www-form-urlencoded;charset=UTF-8";

// request interceptor
axios.interceptors.request.use(
    (config) => {
        const token = store.get("token");
        // if (store.getters.token) {
        if (token) {
            config.headers.Authorization = "Bearer " + token;
        }
        return config;
    },
    (error) => {
        console.log('请求use:', error);
        ElMessage({
            message: "网络请求失败，请检查你的网络!!!",
            type: "error",
            duration: 2000,
        });
        return Promise.reject(error);
    },
);


// response interceptor
axios.interceptors.response.use(
    (response) => {
        if (response.status === 200) {
            //   console.log(response.data);
            if (response.data.success) {
                return Promise.resolve(response);
            } else {
                ElMessage({
                    message: response.data.msg,
                    type: "error",
                    duration: 2000,
                });
                return Promise.resolve(response);
            }
        } else {

            return Promise.reject(response);
        }
    },
    (error) => {
        if (error && error.response) {
            //   401未登录，403token过期
            if (error.response.status == 401) {
                error.message = "错误请求";

                ElMessage({
                    message: "登录过期，请重新登录！！！",
                    type: "error",
                    duration: 2000,
                });

            } else if (error.response.status == 403) {
                //   登录过期
                ElMessage({
                    message: "登录过期，请重新登录！！！",
                    type: "error",
                    duration: 2000,
                });

            } else {
                ElMessage({
                    message: error.response.data.message,
                    type: "error",
                    duration: 2000,
                });
            }
        }
        return Promise.reject(error);
    },
);

let request = {};
request.get = function (url, params) {
    return new Promise((resolve, reject) => {
        axios.get(url, params)
            .then((response) => {
                resolve(response);
            }).catch((err) => {
                reject(err);
            });
    });
};

request.post = function (url, params) {
    return new Promise((resolve, reject) => {
        axios
            .post(url, params)
            .then((res) => {
                resolve(res.data);
            })
            .catch((err) => {

                reject(err);
            });
    }).catch((e) => { });

};

request.getHeaders = function () {
    const token = store.get("token");
    if (token) {
        return {
            authorization: 'Bearer ' + token
        }
    } else {
        return {};
    }
}


export default request;
